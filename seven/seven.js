 ///<reference path="angular.min.js" /> 

var myApp = angular.module('myApp',[]);
	
	myApp.controller("AllCDController", function($scope){   
	$scope.message="hello message";
	});
	
	myApp.controller("CDController", function($scope){   
		
		$scope.cde=[{'title':'bollywood Hits','language':'hindi','price':50},
			{'title':'hollywood Hits','language':'English','price':70},
			{'title':'tollywood Hits','language':'Tamil','price':20}];
	
	// $scope.getAllDetails= function($scope){
	// 	return $scope.CD;
	// };
	
	$scope.fun = function(CD){
		$scope.cde.push({'title':CD.title,'language':CD.language,'price':CD.price});
		$scope.CD={};
		
	};
	
	myApp.config(function($routeProvider) {
  $routeProvider

  .when('/', {
    templateUrl : 'seven.html',
    controller  : 'CDController'
  })

  .when('/AllCDS', {
    templateUrl : 'AllCDs.html',
    controller  : 'AllCDController'
  })

  .when('/NewCDs', {
    templateUrl : 'NewCDs.html',
    controller  : 'NewCDController'
  })

  .otherwise({redirectTo: '/'});
});
	
	});
